package com.mycompanu.mywebapp.client;

import java.io.Serializable;

public class Evento implements Serializable
{
	private String nomeEvento;
	private String luogo;
	private String descrizione;
	private String[] date;
	
	public Evento(String nomeEvento, String luogo, String descrizione, String[] date)
	{
		this.nomeEvento = nomeEvento;
		this.luogo = luogo;
		this.descrizione = descrizione;
		this.date = date;
	}
	
	public String getNomeEvento()
	{
		return nomeEvento;
	}
	
	public String getLuogo()
	{
		return luogo;
	}
	
	public String getDescrizione()
	{
		return descrizione;
	}
	
	public String[] getDate()
	{
		return date;
	}
	
	public void setDate(String[] date)
	{
		this.date = date;
	}
	
	public String toString()
	{
		return nomeEvento +" "+ luogo +" "+ descrizione ;
	}
}