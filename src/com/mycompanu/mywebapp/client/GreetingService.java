package com.mycompanu.mywebapp.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;


/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService
{

	String inserisciUtente(String name) throws IllegalArgumentException;

	String controlloUsername(String name) throws IllegalArgumentException;
	
	String controlloEvento(String idEvento) throws IllegalArgumentException;

	String inserisciEvento(String evento) throws IllegalArgumentException;
	
	String prendiEvento(String idEvento) throws IllegalArgumentException;
	
	String[] getDati(String idEvento) throws IllegalArgumentException;	
	
	String[] idEventi(String username) throws IllegalArgumentException;

	String vota(String all) throws IllegalArgumentException;

	String[] creaMapSondaggi(String sondaggi[]);

	String[] aggiungiCommenti(String commenti[]);

	String[] settaCommenti(String idEvento);

	String controllaChiuso(String idEvento);

	String chiudiEvento(String idEvento);
}
