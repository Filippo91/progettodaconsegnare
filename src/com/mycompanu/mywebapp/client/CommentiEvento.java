package com.mycompanu.mywebapp.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.TextBox;

public class CommentiEvento extends Composite
{
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private static CommentiEventoUiBinder uiBinder = GWT.create(CommentiEventoUiBinder.class);
	
	String username;
	int livSic;
	private String idEvento;
	private String[] commenti;

	@UiField Button indietro;
	@UiField TextBox aggiungi_comm;
	@UiField Button aggiugi;
	@UiField TextBox mostra_commenti;
	@UiField Button clear;
	@UiField TextBox nome_utente;
	@UiField Button visualizza;

	interface CommentiEventoUiBinder extends UiBinder<Widget, CommentiEvento>
	{
	}

	public CommentiEvento()
	{
		initWidget(uiBinder.createAndBindUi(this));
		nome_utente.setText(username);
	}
	
	void setDati(String username,int livSic, String idEvento)
	{
		this.username=username;
		this.livSic=livSic;
		this.idEvento=idEvento;
		nome_utente.setText(username);
		try
		{
			setCommenti();
			System.out.println("Commenti settati");
		}
		catch(Exception e)
		{
			System.out.println("setCommenti non ha funzioanto");
		}
	}
	
	@UiHandler("indietro")
	void onIndietroClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		InfoEvento obj = new InfoEvento();
		obj.setDati(username, livSic, idEvento);
		root.clear();
		root.add(obj);
	}
	
	@UiHandler("aggiugi")
	void onAggiugiClick(ClickEvent event)
	{
		String[] comm = new String[3];
		if(aggiungi_comm.getText()!=null)
		{
			comm[0]=username;
			comm[1]=aggiungi_comm.getText();
			comm[2]=idEvento;
			mostra_commenti.setText(username + " " + comm[1]);
			greetingService.aggiungiCommenti(comm, new AsyncCallback<String[]>()
			{
				public void onFailure(Throwable caught)
				{
					System.out.println("problema sull'aggiungiCommento");
				}
				public void onSuccess(String[] result)
				{
				}
			});
		}
		RootPanel root = RootPanel.get();
		InfoEvento obj = new InfoEvento();
		obj.setDati(username, livSic, idEvento);
		root.clear();
		root.add(obj);
	}
	
	@UiHandler("clear")
	void onClearClick(ClickEvent event)
	{
		aggiungi_comm.setText("");
	}
	
	void setCommenti()
	{
		greetingService.settaCommenti(idEvento,  new AsyncCallback<String[]>()
		{
			public void onFailure(Throwable caught)
			{
				System.out.println("Errore nel settaggio dei commenti");
			}
			public void onSuccess(String[] result)
			{
				commenti=result;
			}
		});
	}
	
	void superSetComm ()
	{
		System.out.println("di nuovo nel client, ora setto i commenti");
		try
		{
			String daStampare=(commenti[0]);
			for(int i=1; i< commenti.length;i++)
			{
				daStampare = daStampare + "  -  " + commenti[i];
			}
			mostra_commenti.setText(daStampare);
		}
		catch(Exception e)
		{
			mostra_commenti.setText("Non sono presenti commenti");
		}
	}

	@UiHandler("visualizza")
	void onVisualizzaClick(ClickEvent event)
	{
		superSetComm();
	}
}