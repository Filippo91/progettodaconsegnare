package com.mycompanu.mywebapp.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.TextBox;

public class CreaEvento extends Composite
{
	private static CreaEventoUiBinder uiBinder = GWT.create(CreaEventoUiBinder.class);
	private String username,nome,luogo,descrizione;
	private int livSic;
	
	@UiField Button vai_gestisci_eventi;
	@UiField TextBox textbox_nome;
	@UiField TextBox textbox_luogo;
	@UiField TextBox textbox_descrizione;
	@UiField Button salva_dati;
	@UiField Button sondaggio;

	interface CreaEventoUiBinder extends UiBinder<Widget, CreaEvento>
	{
	}

	public CreaEvento()
	{
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	void setText(boolean general)
	{
		sondaggio.setEnabled(general);
	}
	
	void setDati(String username,int livSic)
	{
		this.username = username;
		this.livSic = livSic;
		setText(false);
		System.out.println("username: " + username);
	}
	
	@UiHandler("vai_gestisci_eventi")
	void onVai_gestisci_eventiClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		GestisciEvento obj = new GestisciEvento();
		obj.setDati(username, livSic);
		root.clear();
		root.add(obj);
	}
	
	@UiHandler("salva_dati")
	void onSalva_datiClick(ClickEvent event)
	{
		nome =textbox_nome.getText();
		luogo = textbox_luogo.getText();
		descrizione = textbox_descrizione.getText();
		sondaggio.setEnabled(true);
	}

	@UiHandler("sondaggio")
	void onSondaggioClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		Sondaggi obj = new Sondaggi();
		obj.setDati(username, livSic, nome, luogo, descrizione);
		root.clear();
		root.add(obj);
	}
}