package com.mycompanu.mywebapp.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.ListBox;

public class Sondaggi extends Composite
{
	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private static SondaggiUiBinder uiBinder = GWT.create(SondaggiUiBinder.class);
	
	private String username;
	private int livSic;
	private String nome;
	private String luogo;
	private String descrizione;
	private String totale = null;
	private ArrayList<String> date = new ArrayList<String>();
	
	@UiField Button torna_crea_evento;
	@UiField Button aggiungi;
	@UiField Button cancella;
	@UiField Button salva_modifiche;
	@UiField TextBox ora;
	@UiField TextBox giorno;
	@UiField TextBox nome_evento;
	@UiField ListBox elenco_date;
	@UiField TextBox luogo_evento;

	interface SondaggiUiBinder extends UiBinder<Widget, Sondaggi>
	{
	}

	public Sondaggi()
	{
		initWidget(uiBinder.createAndBindUi(this));
	}
	
	void setDati(String username,int livSic, String nome, String luogo, String descrizione)
	{
		this.username=username;
		this.livSic=livSic;
		this.nome=nome;
		this.luogo=luogo;
		this.descrizione= descrizione;
		luogo_evento.setText(luogo);
		nome_evento.setText(nome);
		System.out.println(username);
	}
	
	@UiHandler("torna_crea_evento")
	void onTorna_crea_eventoClick(ClickEvent event)
	{
		RootPanel root = RootPanel.get();
		CreaEvento obj = new CreaEvento();
		obj.setDati(username, livSic);
		root.clear();
		root.add(obj);
	}
	
	@UiHandler("aggiungi")
	void onAggiungiClick(ClickEvent event)
	{
		if((giorno.getText()!=null)&&(ora.getText()!=null))
		{
			System.out.println("pre aggiunta data ed ora");
			date.add(giorno.getText() + " / " + ora.getText());
		}
		aggiornaTabella();		
	}
	
	void aggiornaTabella ()
	{
		elenco_date.clear();
		for(int i =0 ; i< date.size() ; i++)
		{
			elenco_date.addItem(date.get(i));
		}
	}
	
	@UiHandler("cancella")
	void onCancellaClick(ClickEvent event)
	{
		int selezionata = elenco_date.getSelectedIndex();
		if(selezionata!=-1)
		{
			date.remove(selezionata);
		}
		aggiornaTabella();
	}
	
	@UiHandler("salva_modifiche")
	void onSalva_modificheClick(ClickEvent event)
	{
		totale = nome +" , "+luogo+" , "+descrizione+" , "+username;
		String[] sondaggi = new String[date.size()];
		for(int i = 0; i < date.size(); i++)
		{
			sondaggi[i] = date.get(i);
		}
		/*
		 * Scomposizione dell array di stringa contenente i dati dei sondaggi
		 * usiamo un identificatore di 'separazione' diversa '-'
		 */
		totale = totale + " - " + sondaggi[0];
		for (int i = 1; i < sondaggi.length; i++)
		{
			totale = totale + " , " + sondaggi[i];
		}
		
		greetingService.inserisciEvento(totale, new  AsyncCallback<String>()
		{
			public void onFailure(Throwable caught)
			{
				System.out.println("Errore in crea evento, in particolare registra evento");
			}
			public void onSuccess(String result)
			{
				System.out.println("Evento creato");
				RootPanel root = RootPanel.get();
				GestisciEvento obj = new GestisciEvento();
				obj.setDati(result, livSic);
				root.clear();
				root.add(obj);
			}
		});
	}
}